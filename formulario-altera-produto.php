<?php include("cabecalho.php"); 
include("conecta.php");
include("banco_categoria.php");
include("banco-produto.php");
$categorias = listaCategorias($conexao);

$idProduto = (int)$_GET['id'];

$produto = selecionarProduto($conexao, $idProduto);

?>
<h1>Alterar produto</h1>
<form method='POST' action="altera-produto.php">
    <table class="table">
        <tr>
            <td>Nome</td>
            <td><input class="form-control" type="text" name="nome" value="<?=$produto['nome']?>" required/></td>
        </tr>

        <tr>
            <td>Preço</td>
            <td><input class="form-control" type="number" name="preco" value="<?=$produto['preco']?>" required/></td>
        </tr>

        <tr>
            <td>Descrição</td>
            <td><textarea class="form-control" name="descricao" ><?=$produto['descricao']?></textarea></td>
            
        </tr>
        <td>
            <input type="checkbox" name="usado" <?=$produto['usado'] == 1 ? "value='true'" : "value='false'" ?> <?= $produto['usado'] == 1 ? "checked" : "" ?>> Usado

        </td>
        </tr>

        <tr>
            <td>Categoria</td>
            <td>
                <select required class='form-control' name='categoria_id'>
                <option value="">---------</option>
                <?php foreach($categorias as $categoria) : ?>
                <option value=<?= (int) $categoria['id']?> <?= $produto['categoria_id'] == $categoria['id'] ? "selected" : "" ?>>
                    <?=$categoria['nome']?>
                </option>
                <?php endforeach ?>
                </select>
            </td>   
        </tr>
    </table>

    <div class='text-left botaocadastra'>
            <button class="btn btn-primary " type="submit" name="idProduto" value=<?=$idProduto?>>Alterar</button>
            <a class="btn btn-danger " href="produto-lista.php">Cancelar</a>
    </div>
</form>


<?php include("rodape.php"); ?>